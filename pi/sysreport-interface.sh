#!/bin/bash

# sysreport-klient.sh

# Set prefix
prefix="`dirname $0`/"

# Load client name
while [ -z "$user" ];
do
	read -p "Which client do you want?: " user || exit 1
	[ -z "$user" ] && echo "Type name of client of abort using ^C."
done

# Load configuration
. ${prefix}sysreport-interface.conf
list="${tmpdir}/list"

# List availible plugins data
ssh -o Port=${server_ssh_port} ${server_ssh_options} \
    ${server_user}@${server_ip} "ls clientdata/${user}/" \
	> ${list} 2>/dev/null
if [ $? -gt 0 ];
then
	echo "Connection failed or client "$user" does not exist" >&2
	read -n 1 -p "Do you want to try list availible clients? [y/N]: " || exit 1
	echo
	if [[ "$REPLY" = [yY] ]];
	then
		while true;
		do
			ssh -o Port=${server_ssh_port} ${server_ssh_options}  ${server_user}@${server_ip} \
			    "ls clientdata" 2>/dev/null
			if [ $? -gt 0 ];
			then
				echo "Connection failed."
				read -n 1 -p "Abort or Retry? [a/R]: " conerr || exit 1
				echo
				if [[ "${conerr}" = [aA] ]];
				then
					echo "Aborting"
					echo "Bye bye"
					exit 1
				else
					continue
				fi
			fi
			break
		done
	exec $0	
	else
		echo "Bye bye."
		exit 0
	fi
fi

# Test if is plugin data list is empty..
sed '/pkgs/d' ${list} > ${list}2; mv ${list}2 ${list}
[ ! -s "${list}" ] && { echo "Client has no plugin files"; exec $0; }


# Get plugin data and show them
while true;
do

	# Show availible, select and get plugin data
	while true;
	do
		echo "Availible plugins data:"
		cat "${list}" | tr ' ' '\n' | sed 's/^/>\ /'
		echo -n 

		read -p "Chose plugin data to get: " chplugin || exit 1
		cat "$list" | grep -q "^${chplugin}$"

		if [ $? -eq 0 ];
		then
			while true;
			do
				# plugin exists -> downloading it
				scp -Bq -P ${server_ssh_port} ${server_ssh_options} \
				${server_user}@${server_ip}:${server_datadir}/${chplugin} ${tmpdir} || \
				{
					echo "scp can't estabilish connection"
					read -n 1 -p "Abort or Retry? [a/R]: " conerr || exit 1
					echo
					if [[ "${conerr}" = [aA] ]];
					then
						echo "Aborting"
						echo "Bye bye"
						exit 1
					else
						break
					fi
				}
				break
			done
			break
		else
			echo "plugin does not exist"
		fi

	done

	# Check
	echo "Ok, I have this data."
	#ls "${tmpdir}" | tr ' ' '\n' | grep -v list | sed 's/^/>\ /'

	# Present data using gnuplot
	if [ -e "${prefix}/plotscripts/$chplugin" ];
	then
		read -n 1 -p "Use gnuplot to present data? [Y/n]: " || exit 1
		echo
		if [[ "$REPLY" != [nN] ]];
		then
			gnuplot plotscripts/$chplugin
		fi
	fi

	# Show data on screen
	read -n 1 -p "View data? [y/N]: " || exit 1
	echo
	if [[ "$REPLY" = [Yy] ]];
	then
		less ${tmpdir}/${chplugin}
	fi



	# Get another plugin data from another plugin?
	read -n 1 -p "View another plugin data? [y/N]: " || exit 1
	echo
	if [[ "$REPLY" = [Yy] ]];
	then
		continue
	else
		break
	fi

done

# Clean up
rm $tmpdir/*

# Reply for another client?
read -n 1 -p "Retry for another client? [y/N]: " || exit 1
echo
if [[ "$REPLY" = [Yy] ]];
then
	exec $0
fi

# Exit
echo "Bye bye"
