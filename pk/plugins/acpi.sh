#!/bin/bash
# acpi plugin for sysreport-client


prefix=`basename $0 | sed 's/.sh$/:/'`

# making plugin info
echo -n $prefix
echo -n "temperature:"

# get temperature in format XX
# --------EDIT TO GET YOUR TEMPERATURE--------
cat /proc/acpi/thermal_zone/TZ1/temperature | awk '{print $2}'
