#!/bin/bash


# VARIABLES

disk="sda2"


# CODE
prefix=`basename $0 | sed 's/.sh$/:/'`

# Number of processes
echo -n $prefix
echo -n "nproc:"
count=`ps ax | wc -l`
echo $(($count - 1))

# Number of running processes
echo -n $prefix
echo -n "rproc:"
echo $((`ps ax -o state | grep R | wc -l` - 2))

# Average load
echo -n $prefix
echo -n "load:"
cat /proc/loadavg

# Free memory in megabytes
echo -n $prefix
echo -n "fmem:"
free -m | sed -n '3p' | awk '{print $4}'

# Disk usage
echo -n $prefix
echo -n "du:"
df | grep $disk | awk '{print $5}' | sed 's/%//'

# Number of logins by user on vc/x
echo -n $prefix
echo -n "nlogvc:"
last | cut -d" " -f1 | sed '$d' | sed '$d' | sort | uniq -c | awk '{print "("$1":"$2")"}' | tr '\n' ' '; echo

# Kernel name
echo -n $prefix
echo -n "kername:"
uname -s

# Kernel Release
echo -n $prefix
echo -n "kerel:"
uname -r

# Machine
echo -n $prefix
echo -n "machine:"
uname -m

# Processor
echo -n $prefix
echo -n "cpu:"
uname -p

# Platform
echo -n $prefix
echo -n "platf:"
uname -i
