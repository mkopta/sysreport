#!/bin/bash
prefix=`basename $0 | sed 's/.sh$/:/'`

# ---------CONFIGURATION----------
# name of the interface
interface1="eth0"

# --------DO NOT EDIT-------------
# get download and upload bytes
echo -n $prefix
echo -n "${interface1}dl:"
ip -s link show ${interface1} | awk 'NR==4 {print $1}'

echo -n $prefix
echo -n "${interface1}ul:"
ip -s link show ${interface1} | awk 'NR==6 {print $1}'


