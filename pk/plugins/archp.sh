#!/bin/bash
prefix=`basename $0 | sed 's/.sh$/:/'`

# Number of packages
echo -n "${prefix}"
echo -n "pkgs:"
pacman -Q | wc -l

# Last full upgrade
echo -n "$prefix"
echo -n "lastupgrade:"
timeofupgrade=`grep "starting full system upgrade" /var/log/pacman.log | tail -n 1 | sed 's/[^]]*$//;s/\[//;s/\]//'`
expr `date +%s` \- `date --date="$timeofupgrade" +%s`
