#!/bin/bash
# Martin 'dum8d0g' Kopta <martin@kopta.eu> http://martin.kopta.eu
# sysreport-client.sh, version 0.7, May 2008

################################################################
#                                                              #
#                             INIT                             #
#                                                              #
################################################################

# Reset variables (may be inherited from parent enviroment)
# Also overview of used variables
prefix=;sec=;pkg=;LOGFILE=;CONFIGFILE=;VERBOSELEVEL=;pluginsdir=
plugin=;pkgX=;server_ssh_port=;server_conntimeout=;server_user=
server_ip=;server_datadir=;nosend=

# Version sysreport-client
VERSION="0.7"

# Set prefix
prefix="`dirname $0`/"

# Variable 'sec'  is a number of seconds from 1970
# Variable 'logtime' is used for log
sec="`date +%s`"
logtime="`date -d "1970-01-01 $sec sec" "+[%Y-%m-%d %H:%M:%S]"`"

# Default verbose level
VERBOSELEVEL="1"

################################################################
#                                                              #
#                          FUNCTIONS                           #
#                                                              #
################################################################

# check()
# Check everything what sysreport-client needs
# If is all OK, return 0, otherwise 1
function check()
{
	#
	# Base
	#
	if [ ! -f /proc/uptime ]
	then
		echo "File /proc/uptime does not exists!" >&2
		return 1
	fi
	if [ ! -r /proc/uptime ]
	then
		echo "Can not read /proc/uptime!" >&2
		return 1
	fi
	#
	# Plugins directory
	#
	if [ ! -e "$pluginsdir" ]
	then
		echo "Plugins directory \"$pluginsdir\" does not exists!" >&2
		return 1
	fi
	if [ ! -d "$pluginsdir" ]
	then
		echo "Plugins directory \"$pluginsdir\" is not a directory!" >&2
		return 1
	fi
	if [ ! -r "$pluginsdir" ]
	then
		echo "Plugins directory \"$pluginsdir\" is not a readable!" >&2
		return 1
	fi
	if [ ! -x "$pluginsdir" ]
	then
		echo "Plugins directory \"$pluginsdir\" is not a executable!" >&2
		return 1
	fi
	#
	# Datadir
	#
	if [ ! -e "$datadir" ]
	then
		echo "Data directory \"$datadir\" does not exists!" >&2
		return 1
	fi
	if [ ! -d "$datadir" ]
	then
		echo "Data directory \"$datadir\" is not a directory!" >&2
		return 1
	fi
	if [ ! -w "$datadir" ]
	then
		echo "Data directory \"$datadir\" is not writeable!" >&2
		return 1
	fi
	#
	# Archivedir
	#
	if [ ! -e "$archivedir" ]
	then
		echo "Archive directory \"$archivedir\" does not exists!" >&2
		return 1
	fi
	if [ ! -d "$archivedir" ]
	then
		echo "Archive directory \"$archivedir\" is not a directory!" >&2
		return 1
	fi
	if [ ! -w "$archivedir" ]
	then
		echo "Archive directory \"$archivedir\" is not writeable!" >&2
		return 1
	fi
	#
	# Logfile
	#
	if [ -z "$LOGFILE" ]
	then
		echo "Log file is not set!" >&2
		return 1
	fi
	if [ ! -e "$LOGFILE" ]
	then
		echo "Log file \"$LOGFILE\" does not exists!" >&2
		return 1
	fi
	if [ ! -f "$LOGFILE" ]
	then
		echo "Log file \"$LOGFILE\" is not a file!" >&2
		return 1
	fi
	if [ ! -w "$LOGFILE" ]
	then
		echo "Log file \"$LOGFILE\" is not writeable!" >&2
		return 1
	fi
	#
	# Server configuration
	#
	if [ -z "$server_ip" ]
	then
		echo "Server IP must not be empty!" >&2
		return 1
	fi
	if [ -z "$server_ssh_port" ]
	then
		# set to default
		server_ssh_port="22"
	fi
	if [[ ! "$server_ssh_port" =~ ^[0-9]*$ ]]
	then
		echo "Server ssh port must be a number!" >&2
		return 1
	fi
	if [ -z "$server_conntimeout" ]
	then
		echo "Timeout must not be empty!" >&2
		return 1
	fi
	if [[ ! "$server_conntimeout" =~ ^[0-9]*$ ]]
	then
		echo "Timeout must be a number!" >&2
		return 1
	fi
	if [ -z "$server_user" ]
	then
		echo "Server user must not be empty!" >&2
		return 1
	fi
}

# getdata()
# Function getdata get data from client computer and write them into data package
# Return 0 if is succesfull, otherwise 1
function getdata()
{
	# Data package init
	log II "Creating package ${pkg}"
	if [ -f "${pkg}" ]
	then
		log WW "File ${pkg} already exists. Overwriting. May indicate bad usage of program."
	fi
	touch "${pkg}"
	if [ $? != 0 ]
	then
		log EE "Can not create file \"${pkg}\"! Please check ownership and rights."
		return 1
	fi

	log II "Getting metadata"
	# Getting metadata
	echo "meta:hostname:`uname -n`" >> "$pkg"
	echo "meta:uptime:`cut -d. -f1 /proc/uptime`" >> "$pkg"
	echo "meta:date:`date -R`" >> "$pkg"

	log II "Pluginsdir is \"${pluginsdir}\""
	# Executing plugins
	for plugin in "${pluginsdir}/"*
	do
		if checkplugin "${plugin}"
		then
			log II "Executing plugin \"${plugin}\""
			${plugin} >> "$pkg"
		else
			log WW "Bad plugin \"${plugin}\""
		fi
	done

	log II "Getting data done"

	return 0
}

# checkplugin()
# Test a plugin file for file, read, executable
# If is file, read and executable, return 0, otherwise 1
function checkplugin()
{
	plugin="$1"
	if [ ! -f "${plugin}" ]
	then
		log WW "Plugin \"${plugin}\" is not a regular file!"
		return 1
	fi
	if [ ! -r "${plugin}" ]
	then
		log WW "Plugin \"${plugin}\" is not readable!"
		return 1
	fi
	if [ ! -x "${plugin}" ]
	then
		log WW "Plugin \"${plugin}\" is not executable!"
		return 1
	fi

	return 0
}

# send()
# Try to send all unsent data packages to server.
# In case of fail, just pass with warning and return 1
# In case of success move sented package to archive directory
# and return 0.
function send()
{
	# For all packages which are not in archive (was not sent yet)
	for pkgX in "${datadir}/"*
	do
		# If is not file, skip it (to avoid archive directory)
		[ ! -f $pkgX ] && continue
		# Info message
		log II "Trying to send package $pkgX.."
		# Upload package to server via ssh using scp
		scp -o Port=${server_ssh_port} -o ConnectTimeout=${server_conntimeout} -Bq \
		    $pkgX $server_user@$server_ip:$server_datadir 2>/dev/null
		# If upload sucess, move package to archive
		if [ $? == 0 ];
		then
			mv "$pkgX" "${archivedir}"
			log II "Sending package $pkgX succeced."
		else
			log WW "Sending package $pkgX failed."
		fi
	done
}

# log()
# If is $LOGFILE set, append a $1 message into it (depends on $LOGLEVEL)
# EE - verbose level 1 or higher
# WW - verbose level 2 or higher
# II - verbose level 3
# -- - in all verbose levels
# EE are errors, WW warnings, II informations, -- are base messages
# Example: You set verbose level to 2 -> in LOGFILE will be only EE and WW
#          messages
function log()
{
	# Tests
	if [ -z "$LOGFILE" ]
	then
		echo "I have no Logfile! Please, configure me first." 
	fi
	if [ ! -f "$LOGFILE" ]
	then
		echo "Logfile \"${LOGFILE}\" is not a regular file!"
	fi
	if [ ! -w "$LOGFILE" ]
	then
		echo "Cannot write to logfile \"${LOGFILE}\" !"
	fi
	# Writing
	if [ $VERBOSELEVEL == 3 ]
	then
		echo "$logtime $1 $2" >> "$LOGFILE"
	elif [ $VERBOSELEVEL == 2 ]
	then
		if [ "$1" == "WW" -o "$1" == "EE" -o "$1" == "--" ]
		then
			echo "$logtime $1 $2" >> "$LOGFILE"
		fi
	else
		if [ "$1" == "EE" -o "$1" == "--" ]
		then
			echo "$logtime $1 $2" >> "$LOGFILE"
		fi
	fi
}

# usage()
# Print usage
function usage()
{
	echo "Usage: `basename $0` options"
	echo "Availible options:"
	echo " -h              print help"
	echo " -u              print usage"
	echo " -v, -V          print version"
	echo " -c CONFIGFILE   set location of configuration file"
	echo " -l LOGFILE      set location of log file (beat value in config file)"
	echo " -d 1|2|3        set verbosity (1 (default)  or 2 or 3)"
	echo " -n              program will not send data to server"
}

# version()
# Print version
function version()
{
	echo "Sysreport-client $VERSION"
}

# loadconf()
# Load configuration file
function loadconf()
{
	# Some tests
	if [ -z "$CONFIGFILE" ]
	then
		echo "Config file not given!" >&2
		usage
		exit 1
	fi
	if [ ! -e "$CONFIGFILE" ]
	then
		echo "Given config file does not exists!" >&2
		exit 1
	fi
	if [ ! -f "$CONFIGFILE" ]
	then
		echo "Given config file is not a regular file!" >&2
		exit 1
	fi
	if [ ! -r "$CONFIGFILE" ]
	then
		echo "Given config file is not readable!" >&2
		exit 1
	fi
	# Parse configuration file
	archivedir=`grep '^archivedir' $CONFIGFILE | tail -n 1 | \
	            sed 's/archivedir[ ]*=[ ]*//;s/"//g'`
	datadir=`grep '^datadir' $CONFIGFILE | tail -n 1 | \
	         sed 's/datadir[ ]*=[ ]*//;s/"//g'`
	pluginsdir=`grep '^pluginsdir' $CONFIGFILE | tail -n 1 | \
	            sed 's/pluginsdir[ ]*=[ ]*//;s/"//g'`
	if [ -z "$LOGFILE" ]
	then
		LOGFILE=`grep '^logfile' $CONFIGFILE | tail -n 1 | \
		         sed 's/logfile[ ]*=[ ]*//;s/"//g'`
	fi
	server_datadir=`grep '^server_datadir' $CONFIGFILE | tail -n 1 | \
	                sed 's/server_datadir[ ]*=[ ]*//;s/"//g'`
	server_ip=`grep '^server_ip' $CONFIGFILE | tail -n 1 | \
	           sed 's/server_ip[ ]*=[ ]*//;s/"//g'`
	server_ssh_port=`grep '^server_ssh_port' $CONFIGFILE | tail -n 1 | \
	                 sed 's/server_ssh_port[ ]*=[ ]*//;s/"//g'`
	server_user=`grep '^server_user' $CONFIGFILE | tail -n 1 | \
	             sed 's/server_user[ ]*=[ ]*//;s/"//g'`
	server_conntimeout=`grep '^server_conntimeout' $CONFIGFILE | \
	                    tail -n 1 | \
	                    sed 's/server_conntimeout[ ]*=[ ]*//;s/"//g'`
}


# trylock()
# Try create a lock file (directory) in /tmp directory
function trylock()
{
	if [ ! -w /tmp ]
	then
		echo "Can not write into /tmp directory!" >&2
		exit 1
	fi

	# Create lock	
	mkdir /tmp/sysreport-client-lockdir
	# Check reaction
	if [ ! $? -eq 0 ]
	then
		if [ -e /tmp/sysreport-client-lockdir ]
		then
			if [ -d /tmp/sysreport-client-lockdir ]
			then
				# already locked
				echo "Directory /tmp/sysreport-client-lockdir exists" \
			         "in system. Sysreport-client is now locked" \
					 "until you remove this directory. But," \
					 "do it only if you know that other" \
					 "sysreport-client is not running already!" \
					 "This collision may indicate a bad usage." >&2
					 exit 1
			else
				# lock exist but is not a dir
				echo "File /tmp/sysreport-client-lockdir exist in" \
			         "system, but is not a directory. You maybe should" \
				     "it and run sysreport-client again." >&2
				     exit 1
			fi
		fi
	fi
	# If program end, delete the lock directory
	trap "rm -rf /tmp/syreport-client-lockdir" SIGTERM SIGKILL \
	SIGHUP SIGQUIT SIGINT EXIT
}

# unlock()
# Try to unlock a lock directory in /tmp
function unlock()
{
	rmdir /tmp/sysreport-client-lockdir
	if [ ! $? -eq 0 ]
	then
		echo "Unlock was unsuccesful. Something bad happend and" \
		     "sysreport-client is unable to remove" >&2
		echo "  /tmp/sysreport-client-lockdir."
		echo "If you are sure about" \
		     "this, you can remove it yourself or if does not exists" \
			 "you can (with bad feeling) ignore this situation." >&2
		exit 1
	fi
}

################################################################
#                                                              #
#                             WORK                             #
#                                                              #
################################################################

# Parse arguments
while getopts ":huvVc:l:d:n" Option
do
	case $Option in
		h     ) version; usage;
		        echo "Written by Martin 'dum8d0g' Kopta"
				echo "<martin@kopta.eu> http://martin.kopta.eu"
				exit
				;;
		u     ) usage; exit ;;
		v | V ) version; exit ;;
		c     ) CONFIGFILE="$OPTARG" ;;
		l     ) LOGFILE="$OPTARG" ;;
		d     ) 
				if [[ "$OPTARG" =~ [123] ]] 
				then
					VERBOSELEVEL=$OPTARG;
				else
					echo "Bad value of verbosity! Have to be 1, 2 or 3."
					exit 1
				fi
				;;
		n     ) nosend="yes" ;;
		*     ) echo "Unknow option \"$OPTARG\""; usage; exit 1;
	esac
done
shift $(($OPTIND - 1))


# Load and check configuration
loadconf
check

if [ $? != 0 ]
then
	echo "Checking configuration failed." >&2
	exit 1
fi

# Lock
trylock

# Variable 'pkg'  is location of new data package
pkg="${datadir}/${sec}"

# Start
log -- "-- start --"

log -- "Working on package $pkg"
log II "Prefix is $prefix"
log II "Package file is $pkg"

getdata
if [ -z "$nosend" ]
then
	send
fi

log -- "-- end --"

# Unlock
unlock

# EOF
