#!/bin/bash

# filetest read
# Test file if it is file and if it is readable
function filetestfr() {
	if [ ! -f "$1" ];
	then
			echo "File $1 is not regular file" >&2
			exit 1
	else
			if [ ! -r "$1" ];
			then
					echo "File $1 has not read permisions" >&2
					exit 1
			fi
	fi
	return 0
	}

# filetest read write
# Test file if it is file and if it is readable and if it has write permisions
function filetestfrw() {
	filetestfr "$1"
	if [ ! -w "$1" ]
	then
			echo "File $1 has not write permisions" >&2
			exit 1
	fi
	return 0
}

# dirtest
# Test directory if it is directory and if it is executable
function dirtestdrx() {
	if [ ! -d "$1" ];
	then
			echo "Directory $1 is not directory" >&2
			exit 1
	fi
	if [ ! -r "$1" ];
	then
			echo "Directory $1 has not read permisions" >&2
			exit 1
	fi
	if [ ! -x "$1" ];
	then
			echo "Directory $1 has not executable permisions" >&2
			exit 1
	fi
	return 0
	}

function lockfilecreate() {
	# vytvoreni lockfile kvuli paralelnimu behu skriptu
	if [ -f "$lockfile" ];
	then
			echo "sysreport: Lockfile $lockfile exist - another sysreport running"
			exit 1
	else
			touch "$lockfile"
	fi
	return 0

}
function version() {
	echo "Version is $version"
	return 0
	}

function usage()
{
	echo "Usage: `basename $0` options"
	echo "Availible options:"
	echo " -h              print help"
	echo " -u              print usage"
	echo " -c CONFIGFILE   set location of configuration file"
	echo " -l LOGFILE      set location of log file (beat value in config file)"
	echo " -v 1|2|3        set verbosity (1 (default)  or 2 or 3)"
	return 0
}

function showhelp() {
	usage
	echo "Written by Jiri 'pils' Kadanik"
	echo "<jiri@kadanik.org> http://kadanik.org"
	return 0
}

function parseconfigfile() {
	datadir=`grep '^datadir' $configfile | tail -n 1 | sed 's/datadir[ ]*=[ ]*"//;s/"//'`
	pkgdir=`grep '^pkgdir' $configfile | tail -n 1 | sed 's/pkgdir[ ]*=[ ]*"//;s/"//'`
	archivedir=`grep '^archivedir' $configfile | tail -n 1 | sed 's/archivedir[ ]*=[ ]*"//;s/"//'`
	pluginsdir=`grep '^pluginsdir' $configfile | tail -n 1 | sed 's/pluginsdir[ ]*=[ ]*"//;s/"//'`

	if [ -z "$logfile" ]
	then
	logfile=`grep '^logfile' $configfile | tail -n 1 | sed 's/logfile[ ]*=[ ]*"//;s/"//'`
	fi
	if [ -z "$verbosity" ]
	then
	verbosity=`grep '^verbosity' $configfile | tail -n 1 | sed 's/verbosity[ ]*=[ ]*"//;s/"//'`
	fi
	return 0
}

# datadir hierarchy test
function datadirtests() {
	if [ ! -d "$datadir" ]
	then
		echo "Data directory \"$datadir\" is not a directory!" >&2
		exit 1
	fi
	if [ ! -w "$datadir" -o ! -r "$datadir" ]
	then
		echo "Data directory \"$datadir\" has not write or read permisions!" >&2
		exit 1
	fi
	if [ ! -x "$datadir" ]
	then
		echo "Data directory \"$datadir\" has not executable permisions!" >&2
		exit 1
	fi

}

function isdirempty() {
	if [ $(ls -1 "$1" | wc -l) -eq 0 ]
	then
		return 0
	else
		return 1
	fi
}
