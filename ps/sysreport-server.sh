#!/bin/bash
#
# sysreport-server.sh
#
# Hlavni spousteci soubor casti
# server programu sysreport-server

#           EDIT HERE
# ------------------------------  
# adresar s funkcemi
functionsfile="functions.sh"

lockfile="sysreport.lock"

configfile="sysreport-server.conf"


#        DO NOT EDIT HERE
# ------------------------------  
version="0.7"
procesedpackages="0"

prefix="`dirname $0`/"
time=`date +"[%Y-%m-%d %H:%M:%S]"`

# Skoci do adresare se skriptem
cd "$prefix"

# Nacteni souboru s funkcemi
if [ ! -f "${functionsfile}" -o ! -r "${functionsfile}" ];
then
	echo "sysreport: Functions file ${functionsfile} not found" >&2
   	exit 1
fi
. "${functionsfile}"

# Rozpoznani argumentu
while getopts ":huVv:c:l:" Option
do
	case $Option in
		h )	showhelp; exit ;;
		u )	usage; exit ;;
		V )	version; exit ;;
		c )	filetestfr "$OPTARG" &&	configfile="$OPTARG" ;;
		l ) filetestfrw "$OPTARG" && logfile="$OPTARG" ;;
		v ) if [[ "$OPTARG" =~ [123] ]]
				then
					verbosity=$OPTARG
				else
					echo "Bad value for verbosity! Should be 1,2 or 3."
					exit 1
				fi
				;;

		* )	echo "Unknown option $OPTARG"
				usage; exit 1;

		
	esac
done
shift $(($OPTIND -1))

# Nacteni konfigurace serveru
if [ -z "$configfile" ]
then
				echo "Config file not given!">&2
				usage
				exit 1
fi
filetestfr "$configfile"
parseconfigfile

# Nahrani funkci skriptu z plugin adresare
# test adresare
dirtestdrx "${pluginsdir}"

# test jestli jsou vubec nejake pluginy pro praci
if isdirempty "$pluginsdir"
then
	echo "You do not have any plugins" >&2
	exit 1
fi

# nacteni pluginu
for plugin in "${pluginsdir}/"*
do
	. "${plugin}"
done

# testovani adresare s daty
dirtestdrx "${datadir}"
if [ ! -w "$datadir" ]
then
	echo "Directory $datadir has not write permisions" >&2
	exit 1
fi

[ "$verbosity" == "2" ] && echo "$time: -- start --" >> "$logfile"
[ "$verbosity" == "2" ] && echo "$time: datadir is $datadir" >> "$logfile"

# test jestli vubec jsou nejaci klienti v adresari
if isdirempty "$datadir"
then
	echo "You do not have any client directories"
	exit 0
fi

# Vytvoreni lockfile souboru
lockfilecreate

# vlastni zpracovani balicku od klientu
for client in "${datadir}/"* 
do
	# testovani adresaru s klienty
	if [ ! -d "$client" ]
	then
		[ "$verbosity" -ge "1" ] && echo "$time: Client $client is not client directory but some file" >> "$logfile"
		continue
	fi
	if [ ! -w "$client" -o ! -r "$client" -o ! -x "$client" ]
	then
		[ "$verbosity" -ge "1" ] && echo "$time: Client dir $client do not have rwx permision" >> "$logfile"
		continue
	fi

	client=`basename $client`
	[ "$verbosity" == "2" ] && echo "$time: processing data of client $client" >> "$logfile"
	[ "$verbosity" == "2" ] && echo "$time:  changing directory to $datadir/$client" >> "$logfile"

	cd "${datadir}/${client}"

	# test, jestli jsou vubec nejake balicky na zpracovani
	if isdirempty "$pkgdir"
	then
		[ "$verbosity" == "2" ] && echo "$time:  there are no packages for client $client to do" >> "../../$logfile"
		exit 0
	fi

	# forcyklus pro zpracovani vsech balicku v adresari klienta
	for package in "${pkgdir}/"*;
	do
		[ "$verbosity" == "2" ] && echo "$time:  processing package $package" >> "../../$logfile"
		[ ! -f "$package" -o ! -r "$package" ]  && continue

		# pocet zpracovanych balicku (pro info do logu)
		procesedpackages=$((procesedpackages++))

		# pro vsechny balicky z adresare se provede
		# zpracovani balicku - zavolani vsech pluginu
		for plugin in "../../${pluginsdir}/"*;
		do
			plugin=`basename "$plugin"`
			[ "$verbosity" == "2" ] && echo "$time:   executing plugin $plugin" >> "../../$logfile"
			"$plugin" "$package"
		done

		[ "$verbosity" == "2" ] && echo "$time:   moving package $package to archive" >> "../../$logfile"

		# test zapisu do archive adresare
		if [ -d "$archivedir" -a -w "$archivedir" ]
		then
			mv "$package" "$archivedir/`basename $package`"
			else
				[ "$verbosity" == "2" ] && echo "$time:   can not write to archivedir" >> "../../$logfile"
				rm "$package"
		fi
				
	done

	# vyskoci se z jejich adresare a skoci se do dalsiho prubehu cyklu (+ zahozeni hlasky pri cd -)
	cd - >/dev/null
done

[ "$verbosity" -ge "1" ] && echo "$time: Packages done ${procesedpackages} " >> "$logfile"
[ "$verbosity" == "2" ] && echo "$time: -- end --" >> "$logfile"

# smazani zamku
rm -f "$lockfile"
